Use a single Shelly wall switch to control all your shelly devices!

Each button is associated with a shelly Shelly and depending on the model you can do different operations.

Supported Shellys:
- Every model with relays
- RGBW