const long_detect_s = 0.250;
const shelly_sel_s = 1;
const min_led_on_s = 0.500;
const zerocem_factor = 20;
const op_modes = {"sw":0, "bright": 1}
let shellys = [
  {"ip":"192.168.42.100", "type":0, "led_states": [false,false,false,false], "on_times": [0,0,0,0], "bright": [0,0,0,0]},
  {"ip":"192.168.42.94", "type":0, "led_states": [false,false,false,false], "on_times": [0,0,0,0], "bright": [0,0,0,0]},
  {"ip":"192.168.42.39", "type":1, "led_states": [false,false,false,false], "on_times": [0,0,0,0]}
];
let sw_on_times = [0,0,0,0];
let sw_off_times = [0,0,0,0];
let updowndim = [true, true, true, true];
// let multi_detections = [0,0,0,0];
let long_press_detect = [false,false,false,false];
let long_press_duration = [0,0,0,0];
let op_mode = {"sw":0, "mode": op_modes.sw};
let selected_shelly = 0;
let selected_time = 0;
let curr_shelly=shellys[selected_shelly];

function event_handle(event) {
  try {
    //********************************************* Switch press *********************************************/
    if (event.name === "input" && event.info.state === true) {
      // print("EVENT: ",event);
      // if(sw_on_times === 0 || sw_on_times - multi_detections[event.id]) {
      //   multi_detections[event.id]++;
      // } else {
      //   multi_detections[event.id]=1;
      // }
      sw_on_times=event.info.ts;
      curr_shelly=shellys[selected_shelly];
      print("curr_shelly: ", curr_shelly);
      switch(op_mode.mode) {
        case op_modes.sw:
          switch(curr_shelly.type) {
          case 0:
              Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/white/"+event.id.toString()},
                function (res, error_code, error_msg, ud) {
                  if(res && res.code === 200){
                    let body = JSON.parse(res.body);
                    // print("BODY: ",body);
                    curr_shelly.bright[event.id] = body.brightness;
                    curr_shelly.led_states[event.id] = body.ison;
                    
                  } else {
                    print("Error getting curr_shelly status!");
                    print("http_ctrl - res: ", res);
                    print("http_ctrl - error_code: ", error_code);
                    print("http_ctrl - error_msg: ", error_msg);
                    print("http_ctrl - ud: ", ud);
                    return;
                  }
                }
              ,null);
              break;
            case 1:
              Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/relay/"+event.id.toString()},
                function (res, error_code, error_msg, ud) {
                  if(res && res.code === 200){
                    let body = JSON.parse(res.body);
                    // print("BODY: ",body);
                    curr_shelly.led_states[event.id] = body.ison;
                  } else {
                    print("Error toggling switch!");
                    print("http_ctrl - res: ", res);
                    print("http_ctrl - error_code: ", error_code);
                    print("http_ctrl - error_msg: ", error_msg);
                    print("http_ctrl - ud: ", ud);
                    return;
                  }
                }
              ,null);
              break;
            case 2:
              break;
            default:
              break;
          }
          break;
        case op_modes.bright:
          
          break;
        default:
          break;
      }
      
      //********************************************* Switch release *********************************************/
    } else if (event.name === "input" && event.info.state === false) {
      print("Switch was on for ",(event.info.ts-sw_on_times));
      long_press_detect[event.id] = (((event.info.ts-sw_on_times) > long_detect_s));
      if (long_press_detect[event.id]) {
        long_press_duration[event.id] = event.info.ts-sw_on_times;
      }
      switch(op_mode.mode) {
        case op_modes.sw:
          // if(long_press_duration[event.id] > 5) {
          if(long_press_detect[event.id] && long_press_duration[event.id] > shelly_sel_s) {
            // if(selected_time === 0 || Date.now()-selected_time > 5000) {
              selected_time = Date.now();
              selected_shelly = (selected_shelly + 1)%shellys.length;
              print("Selected curr_shelly: ", curr_shelly.ip);
              print("LONG_PRESS: ",long_press_duration[event.id]);
              return;
            // }
          }
          print("curr_shelly! ",curr_shelly);
          switch(curr_shelly.type) {
            case 0:
              print("PIU!");
              if (long_press_detect[event.id]) {
                  op_mode.sw=event.id;
                  op_mode.mode = op_modes.bright;
              } else if(curr_shelly.led_states[event.id]) {
                Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/white/"+event.id.toString()+"?turn=off"},
                  function (res, error_code, error_msg, ud) {
                    if(res && res.code === 200) {
                      print("Turning LEDs ",event.id," OFF!");
                      curr_shelly.led_states[event.id] = false;
                    } else {
                      print("Error on HTTP turning off curr_shelly!");
                      print("http_ctrl - res: ", res);
                      print("http_ctrl - error_code: ", error_code);
                      print("http_ctrl - error_msg: ", error_msg);
                      print("http_ctrl - ud: ", ud);
                      return;
                    }
                  }  
                ,null);
              } else if (!curr_shelly.led_states[event.id]) {
                // print("LEDs were on for :",event.info.ts-led_on_times[event.id]/1000);
                Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/white/"+event.id.toString()+"?turn=on"},
                  function (res, error_code, error_msg, ud) {
                    if(res && res.code === 200){
                      curr_shelly.led_states[event.id] = true;
                      curr_shelly.on_times[event.id] = Date.now();
                      print("Turning LEDs ",event.id," ON!");
                    } else {
                      print("Error on HTTP turning on curr_shelly!");
                      print("http_ctrl - res: ", res);
                      print("http_ctrl - error_code: ", error_code);
                      print("http_ctrl - error_msg: ", error_msg);
                      print("http_ctrl - ud: ", ud);
                      return;
                    }
                  }
                ,null);
              }
              break;
            case 1:
              if (curr_shelly.led_states[event.id]) {
                Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/relay/"+event.id.toString()+"?turn=off"},
                  function (res, error_code, error_msg, ud) {
                    if(res && res.code === 200) {
                      print("Turning switch ",event.id," off!");
                      curr_shelly.led_states[event.id] = false;
                    } else {
                      print("Error on HTTP turning off switch!");
                      print("http_ctrl - res: ", res);
                      print("http_ctrl - error_code: ", error_code);
                      print("http_ctrl - error_msg: ", error_msg);
                      print("http_ctrl - ud: ", ud);
                      return;
                    }
                  }  
                ,null);
              } else if(!curr_shelly.led_states[event.id]){
                  Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/relay/"+event.id.toString()+"?turn=on"},
                    function (res, error_code, error_msg, ud) {
                      if(res && res.code === 200){
                        curr_shelly.led_states[event.id] = true;
                        // print("Success toggling switch");
                      } else {
                        print("Error toggling switch!");
                        print("http_ctrl - res: ", res);
                        print("http_ctrl - error_code: ", error_code);
                        print("http_ctrl - error_msg: ", error_msg);
                        print("http_ctrl - ud: ", ud);
                        return;
                      }
                    }
                  ,null);
              }
              break;  
            default:
              break;
          }
          
          break;
        case op_modes.bright:
          if(op_mode.sw === event.id) {
            op_mode.mode = op_modes.sw;
            return;
          }
          switch(op_mode.sw) {
            case 0:
              if(event.id === 1) {
                updowndim[op_mode.sw] = true;
              } else if(event.id === 2) {
                updowndim[op_mode.sw] = false;
              } else {
                return;
              }
              break;
            case 1:
              if(event.id === 0) {
                updowndim[op_mode.sw] = true;
              } else if(event.id === 3) {
                updowndim[op_mode.sw] = false;
              } else {
                return;
              }
              break;
            case 2:
              if(event.id === 0) {
                updowndim[op_mode.sw] = true;
              } else if(event.id === 3) {
                updowndim[op_mode.sw] = false;
              } else {
                return;
              }
              break;
            case 3:
              if(event.id === 1) {
                updowndim[op_mode.sw] = true;
              } else if(event.id === 2) {
                updowndim[op_mode.sw] = false;
              } else {
                return;
              }
              break;
          }
          let increment = Math.round((event.info.ts-sw_on_times)*zerocem_factor);
          let before_dimming=curr_shelly.bright[op_mode.sw];
          if(curr_shelly.bright[op_mode.sw]<100 && updowndim[op_mode.sw]) {
            if(curr_shelly.bright[op_mode.sw]+increment > 100) {
              curr_shelly.bright[op_mode.sw] = 100;
              updowndim[op_mode.sw] = false;
            } else {
              curr_shelly.bright[op_mode.sw]+=increment;
            }
          } else if(curr_shelly.bright[op_mode.sw]>1 && !updowndim[op_mode.sw]) {
            if(curr_shelly.bright[op_mode.sw]-increment < 1) {
              curr_shelly.bright[op_mode.sw] = 1;
              updowndim[op_mode.sw] = true;
            } else {
              curr_shelly.bright[op_mode.sw]-=increment;
            }
          } else {
            print("curr_shelly: ",curr_shelly);
            print("updowndim: ", updowndim[op_mode.sw]);
            print("op_mode.sw: ", op_mode);
          } 
          Shelly.call("HTTP.GET",{url: "http://"+curr_shelly.ip+"/white/"+op_mode.sw.toString()+"?brightness="+curr_shelly.bright[op_mode.sw].toString()},
            function (res, error_code, error_msg, ud) {
              if(!res || res.code !== 200){
                curr_shelly.bright[op_mode.sw]=before_dimming;
                print("Error on HTTP control!");
                print("http_ctrl - res: ", res);
                print("http_ctrl - error_code: ", error_code);
                print("http_ctrl - error_msg: ", error_msg);
                print("http_ctrl - ud: ", ud);
                return;
              }
            }
          ,null);
          break;
        default:
          break;
      }
    }
  } catch (error) {
    print("ERROR running script: ", error);
  }
}

function status_handle(event) {
    print("EVENT: ",event);
}

Shelly.addEventHandler(event_handle);
//Shelly.addStatusHandler(status_handle);